import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IBeacon } from 'ionic-native';

@Component({
  selector: 'page-beacons',
  templateUrl: 'beacons.html'
})
export class BeaconsPage {

	constructor(public navCtrl: NavController) {
	console.log('beacons')

	IBeacon.requestAlwaysAuthorization();

	let delegate = IBeacon.Delegate();
	let beaconRegion = IBeacon.BeaconRegion('i-ed-beacons','B9407F30-F5F8-466E-AFF9-25556B57FE6D');

	delegate.didRangeBeaconsInRegion().subscribe(
		data => {
			document.getElementById('log').innerHTML = 'No data';
			for(let i in data.beacons) { 
				if( data.beacons[i].proximity == 'ProximityImmediate' )
				{
					console.log(data.beacons[i]);
					document.getElementById('log').innerHTML = data.beacons[i].major + ':' + data.beacons[i].minor;
					return;
				}
			}
		},
		error => console.error()
	);

	delegate.didStartMonitoringForRegion().subscribe(
		data => console.log('didStartMonitoringForRegion: ', data.beacons),
		error => console.error()
	);

	delegate.didEnterRegion().subscribe(
		data => {
		  console.log('didEnterRegion: ', data)
		}
	);

	IBeacon.startRangingBeaconsInRegion(beaconRegion)
		.then(
			() => console.log('Native layer recieved the request to monitoring'),
			error => console.error('Native layer failed to begin monitoring: ', error)
		);
	}

}